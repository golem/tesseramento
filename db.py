# -*- coding: utf-8 -*-

import io
import sqlite3

class Db():

    def __init__(self):
        self.dbfile = "iscritti.sqlite3"

    def insert_new(self, iscritto):
        firma_stream = io.BytesIO()
        iscritto.img_firma.save(firma_stream, format="PNG")
        firma_bytes = firma_stream.getvalue()
        conn = sqlite3.connect(self.dbfile)
        cur = conn.cursor()
        # Come salvo il contenuto della Canvas
        # nel db che non ce' una "textvar" associata?
        par = (
            iscritto.nome.get(),
            iscritto.cognome.get(),
            iscritto.data_nascita.get(),
            iscritto.comune_nascita.get(),
            iscritto.email.get(),
            iscritto.occupazione.get(),
            iscritto.fonte.get(),
            iscritto.data_firma.get(),
            firma_bytes)
        sql = (
            "INSERT INTO iscritti ("
            "nome"
            ", cognome"
            ", data_nascita"
            ", comune_nascita"
            ", email"
            ", occupazione"
            ", fonte"
            ", data_firma"
            ", firma"
            ") values ("
            "?"      # nome
            ", ?"    # cognome
            ", ?"    # data_nascita
            ", ?"    # comune_nascita
            ", ?"    # email
            ", ?"    # occupazione
            ", ?"    # fonte
            ", ?"    # data_firma
            ", ?"    # firma
            ");")
        cur.execute(sql, par)
        conn.commit()
        conn.close()
