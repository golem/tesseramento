#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tkinter as tk

from iscritti import Iscritti

def main():
    root = tk.Tk()
    Iscritti(root)
    root.mainloop()


if __name__ == "__main__":
    main()
