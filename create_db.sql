--
-- File generated with SQLiteStudio v3.2.1 on lun mag 6 20:53:06 2019
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: iscritti
CREATE TABLE iscritti (
    id             INTEGER       PRIMARY KEY AUTOINCREMENT,
    nome           VARCHAR (50)  NOT NULL,
    cognome        VARCHAR (50)  NOT NULL,
    data_nascita   DATE          NOT NULL,
    comune_nascita VARCHAR (255) NOT NULL,
    email          VARCHAR (255) NOT NULL,
    occupazione    VARCHAR (50),
    fonte          VARCHAR (255),
    data_firma     DATE          NOT NULL,
    firma          BLOB          NOT NULL
);

-- Table: sqlite_sequence
CREATE TABLE sqlite_sequence (
    name,
    seq
);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
