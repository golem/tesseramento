# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.messagebox as tkMessagebox

from datetime import datetime

from PIL import Image

from const import MESSAGE_BOX_TITLE

class Iscritto():

    def __init__(self, larghezza_firma, altezza_firma, img_firma_color_fg, img_firma_color_bg):
        self.larghezza_firma = larghezza_firma
        self.altezza_firma = altezza_firma
        self.img_firma_color_fg = img_firma_color_fg
        self.img_firma_color_bg = img_firma_color_bg
        self.id = tk.StringVar()
        self.cognome = tk.StringVar()
        self.nome = tk.StringVar()
        self.data_nascita = tk.StringVar()
        self.comune_nascita = tk.StringVar()
        self.email = tk.StringVar()
        self.occupazione = tk.StringVar()
        self.fonte = tk.StringVar()
        self.data_firma = tk.StringVar()
        # In Image.new(), se non indicato il "color" e' nero
        # "L" = 8 bit - B/N
        self.img_firma = Image.new("L", (self.larghezza_firma, self.altezza_firma), self.img_firma_color_bg)
        return

    def new(self):
        self.id.set("")
        self.cognome.set("")
        self.nome.set("")
        self.data_nascita.set("")
        self.comune_nascita.set("")
        self.email.set("")
        self.occupazione.set("")
        self.fonte.set("")
        self.data_firma.set("")
        # In Image.new(), se non indicato il "color" e' nero
        # "L" = 8 bit - B/N
        self.img_firma = Image.new("L", (self.larghezza_firma, self.altezza_firma), self.img_firma_color_bg)

    def is_valid(self):
        is_valid = self.cognome_is_valid()
        if is_valid:
            is_valid = self.nome_is_valid()
        if is_valid:
            is_valid = self.data_nascita_is_valid()
        if is_valid:
            is_valid = self.comune_nascita_is_valid()
        if is_valid:
            is_valid = self.email_is_valid()
        if is_valid:
            is_valid = self.occupazione_is_valid()
        if is_valid:
            is_valid = self.fonte_is_valid()
        if is_valid:
            is_valid = self.data_firma_is_valid()
        # Non si puo' convalidare la firma che e'
        # una immagine
        return is_valid

    def cognome_is_valid(self):
        is_valid = True
        if len(str.rstrip(self.cognome.get())) == 0:
            is_valid = False
            messaggio = "'Cognome' e' obbligatorio"
            tkMessagebox.showerror(MESSAGE_BOX_TITLE, messaggio)
        return is_valid

    def nome_is_valid(self):
        is_valid = True
        messaggio = ""
        if len(str.rstrip(self.nome.get())) == 0:
            is_valid = False
            messaggio = "'Nome' e' obbligatorio"
            tkMessagebox.showerror(MESSAGE_BOX_TITLE, messaggio)
        return is_valid

    def data_nascita_is_valid(self):
        is_valid = True
        messaggio = ""
        if self.data_nascita.get() is None:
            is_valid = False
            messaggio = "'Data di nascita' e' obbligatorio"
        elif len(str.rstrip(self.data_nascita.get())) == 0:
            is_valid = False
            messaggio = "'Data di nascita' e' obbligatorio"
        else:
            try:
                datetime.strptime(self.data_nascita.get(), '%x')
            except (ValueError):
                is_valid = False
                messaggio = "'Data di nascita' non valida"
        if not is_valid:
            tkMessagebox.showerror(MESSAGE_BOX_TITLE, messaggio)
        return is_valid

    def comune_nascita_is_valid(self):
        is_valid = True
        messaggio = ""
        if len(str.rstrip(self.comune_nascita.get())) == 0:
            is_valid = False
            messaggio = "'Comune di nascita' e' obbligatorio"
            tkMessagebox.showerror(MESSAGE_BOX_TITLE, messaggio)
        return is_valid

    def email_is_valid(self):
        is_valid = True
        messaggio = ""
        if len(str.rstrip(self.email.get())) == 0:
            is_valid = False
            messaggio = "'E-mail' e' obbligatorio"
            tkMessagebox.showerror(MESSAGE_BOX_TITLE, messaggio)
        return is_valid

    def occupazione_is_valid(self):
        # Non obbligatorio
        return True

    def fonte_is_valid(self):
        # Non obbligatorio
        return True

    def data_firma_is_valid(self):
        is_valid = True
        messaggio = ""
        if self.data_firma.get() is None:
            is_valid = False
            messaggio = "'Data firma' e' obbligatorio"
        elif len(str.rstrip(self.data_firma.get())) == 0:
            is_valid = False
            messaggio = "'Data firma' e' obbligatorio"
        else:
            try:
                datetime.strptime(self.data_firma.get(), '%x')
            except (ValueError):
                is_valid = False
                messaggio = "'Data firma' non valida"
        if not is_valid:
            tkMessagebox.showerror(MESSAGE_BOX_TITLE, messaggio)
        return is_valid

