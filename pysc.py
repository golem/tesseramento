#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://www.mmxforge.net/index.php/sviluppo/python/item/9-lettura-dei-dati-della-tessera-sanitaria-con-python

from smartcard.System import readers
import array
import string
import datetime

r = readers()
reader = r[0]
print("Sto usando: {}".format(reader.name))
connection = reader.createConnection()
connection.connect()

#Seleziona del MF
#CLS 00, istruzione A4 (seleziona file), P1 = P2 = 0 (seleziona per ID),
#Lc: 2, Data: 3F00 (id del MF)
SELECT_MF = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x3F, 0x00]
data, sw1, sw2 = connection.transmit(SELECT_MF)
#se tutto è andato a buon fine sw1 e sw2 contengono
#rispettivamente i valori 0x90 e 0x00 il corrispettivo del 200 in HTTP

#Seleziona del DF1...vedi sopra
SELECT_DF1 = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x11, 0x00]
data, sw1, sw2 = connection.transmit(SELECT_DF1)

#Seleziona del file EF.Dati_personali... vedi sopra sopra
SELECT_EF_PERS = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x11, 0x02]
data, sw1, sw2 = connection.transmit(SELECT_EF_PERS)

#leggiamo i dati
#CLS 00, istruzione B0 (leggi i dati binari contenuti nel file
READ_BIN = [0x00, 0xB0, 0x00, 0x00, 0x00, 0x00]
data, sw1, sw2 = connection.transmit(READ_BIN)
#data contiene i dati anagrafici in formato binario
#trasformiamo il tutto in una stringa
stringa_dati_personali = array.array('B', data).tostring()

dimensione = int(stringa_dati_personali[0:6],16)
print("Dimensione in byte dei dati: {}".format( dimensione))
numero_field = 0

prox_field_size = int(stringa_dati_personali[6:8], 16)
da = 8
a = da + prox_field_size
if prox_field_size > 0:
  codice_emettitore = stringa_dati_personali[da:a]
  print("Codice emettitore: {}".format(codice_emettitore.decode("utf-8")))
  numero_field += 1

da = a
a +=2
prox_field_size = int(stringa_dati_personali[da:a], 16)
da=a
a += prox_field_size
if prox_field_size > 0:
  data_rilascio_tessera = stringa_dati_personali[da:a]
  year = int(data_rilascio_tessera[-4:])
  month = int(data_rilascio_tessera[2:4])
  day = int(data_rilascio_tessera[0:2])
  dtm_data_rliascio = datetime.date(year, month, day)
  # print(string("Data rilascio tessera: ", data_rilascio_tessera[0:2], "/", data_rilascio_tessera[2:4], "/", data_rilascio_tessera[-4:])
  print("Data rilascio tessera: {}".format(dtm_data_rliascio))
  numero_field += 1

da = a
a +=2
prox_field_size = int(stringa_dati_personali[da:a], 16)
da=a
a += prox_field_size
if prox_field_size > 0:
  data_scadenza_tessera = stringa_dati_personali[da:a]
  year = int(data_scadenza_tessera[-4:])
  month = int(data_scadenza_tessera[2:4])
  day = int(data_scadenza_tessera[0:2])
  dtm_data_scadenza = datetime.date(year, month, day)
  # print("Data scadenza tessera: ", data_scadenza_tessera[0:2]+"/"+data_scadenza_tessera[2:4]+"/"+data_scadenza_tessera[-4:])
  print("Data scadenza tessera: {}".format(dtm_data_scadenza))
  numero_field += 1

# da = a
# a +=2
# prox_field_size = int(stringa_dati_personali[da:a], 16)
# da=a
# a += prox_field_size
# if prox_field_size > 0:
#   cognome = stringa_dati_personali[da:a]
#   print("Cognome: {}".format(cognome))
#   numero_field += 1

while a < dimensione:
  da = a
  a += 2
  prox_field_size = int(stringa_dati_personali[da:a], 16)
  da=a
  a += prox_field_size
  if prox_field_size > 0:
    field = stringa_dati_personali[da:a]
    print("Field#{}: {}".format(numero_field, field))
    numero_field += 1